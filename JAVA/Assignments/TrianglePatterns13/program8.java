import java.util.*;

class Triangle{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");

		int row = sc.nextInt();

		int num=row;
		int ch= 64+row;

		for(int i=1;i<=row;i++){
		
			for(int j=i;j<=row;j++){
			
				if(i%2==1){
				
					System.out.print(num--+" ");
				}else{
				
					System.out.print((char)ch--+" ");
				}
			}
			num=row-i;
                        ch=64+row-i;
			System.out.println();
		}
	}
}
