import java.io.*;

class Triangle{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br= new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int row= Integer.parseInt(br.readLine());

		int num= row*(row+1);

		for(int i=1;i<=row;i++){
		
			for(int j=i;j<=row;j++){
			
				System.out.print(num+" ");
				num-=2;
			}
			System.out.println();
		}
	}
}
