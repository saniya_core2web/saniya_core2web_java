import java.io.*;

class Triangle{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int row=Integer.parseInt(br.readLine());
		int num=0;

		for(int i= row; i>1;i--){
		
			num=num+i;
		}

		int ch=65+num;

		for(int i=1;i<=row;i++){

			for(int j=i;j<=row;j++){
			
				System.out.print((char)ch--+" ");
				
			}
		
			System.out.println();
		}
	}
}
