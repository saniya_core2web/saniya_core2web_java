import java.util.*;

class Triangle{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
                int row = sc.nextInt();

                for(int i=1;i<=row;i++){

			int ch1=65+row-i;
			int ch2=97+row-i;
                        
			for(int j=i;j<=row;j++){
			
				if((row%2==1 && i%2==1) || (row%2==0 && i%2==0)){
				
					System.out.print((char)ch2--+" ");
				}else{
				
					System.out.print((char)ch1--+" ");
				}

                        }

                        System.out.println();
                }
        }
}
