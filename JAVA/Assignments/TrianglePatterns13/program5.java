import java.io.*;

class Triangle{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int row= Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
		
			char ch1='A';
			char ch2='a';

			for(int j=i;j<=row;j++){
			
				if(i%2==1){
				
					System.out.print(ch1+++" ");
				}else{
				
					System.out.print(ch2+++" ");
				}
			}
			System.out.println();
		}
	}
}
