import java.util.*;

class Triangle{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();
		
		int num=row*(row+1)-1;

		for(int i=1;i<=row;i++){
		
			for(int j=i;j<=row;j++){
			
				System.out.print(num + " ");
				num-=2;
			}

			System.out.println();
		}
	}
}
