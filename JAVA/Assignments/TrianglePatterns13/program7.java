import java.util.*;

class Triangle{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");

		int row = sc.nextInt();

		
		int temp =row;
		int ch=96+row;

		for(int i=1;i<=row;i++){
			
			int cnt=1;	
			for(int j=i;j<=row;j++){
			
				if(cnt%2==1){
				
					System.out.print(temp+" ");

				}else{
				
					System.out.print((char)ch+" ");
				}
				temp--;
				ch--;
				cnt++;
			}
			temp=row-i;
			ch=96+row-i;
			System.out.println();
		}
	}
}
