import java.util.*;

class Triangle{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");

		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
		
			int num=1;
			char ch='a';
			int cnt=1;

			for(int j=i;j<=row;j++){
			
				if(cnt%2==1){
				
					System.out.print(num+++" ");
				}else{
				
					System.out.print(ch+++" ");
				}
				cnt++;
			}
			System.out.println();
		}
	}
}
