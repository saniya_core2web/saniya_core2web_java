import java.io.*;

class Triangle{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		
		int row = Integer.parseInt(br.readLine());
		int num = 0;

		for(int i=1; i<=row;i++){
		
			for(int j=i;j<=row;j++){
				num+=2;
				System.out.print(num+ " ");
			}
			System.out.println();
		}

	}


}
