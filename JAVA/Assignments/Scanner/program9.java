import java.util.*;
class Even{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number1: ");
		int num1=sc.nextInt();

		System.out.print("Enter number2: ");
		int num2=sc.nextInt();

		for(int i=num1; num1<=num2; num1++){
			if(num1%2==0){
				System.out.print(num1 + ",");
			}
		}
		System.out.println();
	}
}
