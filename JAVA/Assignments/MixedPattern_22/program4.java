import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int ch1 = 64+i;
			int ch2 = 96+i;
			for(int j=1; j<=rows; j++){

				if(rows%2==1 && j>=i)
					System.out.print((char)ch1++ + " ");
				else if(rows%2==0 && j>=i)
					System.out.print((char)ch2++ + " ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}
}
