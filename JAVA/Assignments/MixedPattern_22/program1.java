import java.util.*;

class Pattern {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = 1;
		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=1; j--){

				if(j<=i){
					System.out.print(num + "  ");
					num+=2;
				}
				else
					System.out.print("   ");
			}
			System.out.println();
		}
	}
}
