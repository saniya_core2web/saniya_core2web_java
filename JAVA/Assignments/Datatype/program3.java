class Scientist{
	public static void main(String[] args){

		float pi = 3.14f;
		int r = 3;
		int angle = 360;

		System.out.println(pi);
		System.out.println("Area: " + pi*r*r);
		System.out.println("Angle: " + angle);
	}
}
