import java.io.*;

class Pattern{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int row= Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
		
			int ch1=96+row;
			int ch2=64+row;

			for(int j=1;j<=i;j++){
			
				if(i%2==1){
				
					System.out.print((char)ch1--+" ");
				}else{
					
					System.out.print((char)ch2--+" ");
				}
			}
			System.out.println();
		}
	}
}
