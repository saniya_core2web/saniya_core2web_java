import java.util.*;

class Array {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter array size : ");
                int size = sc.nextInt();

                int arr[] = new int[size];
                
		System.out.println("Enter elements : ");
                
		for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();                                                                                                       }
		int temp;

		for(int i=0; i<arr.length; i++){
			for(int j =i+1; j<arr.length; j++){
				if(arr[i]>arr[j]){
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}

		System.out.println("Third largest element is: " + arr[size-3] + " ");
	}
}
