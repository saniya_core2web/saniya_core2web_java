import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

                int size = sc.nextInt();
                
		char arr[]= new char[size];
                
		for(int i=0;i<size;i++){
			System.out.println("Enter characters: ");
                        arr[i]=sc.next().charAt(0);
                }

		System.out.println("Before reverse: ");

		for(int i=0; i<size; i++){
			if(i%2==0){
				System.out.println(arr[i]);
			}
		}

		for(int i=0;i<size/2; i++){
			int temp = arr[i];
			arr[i] = arr[size-1-i];
			arr[size-1-i] = (char)temp;
		}

		System.out.println("After reverse: ");

		for(int i= 0; i<size; i++){
			if(i%2==0){
				System.out.println(arr[i]);
			}
		}
	}

}
