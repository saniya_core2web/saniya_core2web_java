import java.util.*;

class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();
		
		int arr[]= new int[size];
		
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int sum1=0;
		int sum2=0;

		for(int i=0;i<size;i++){
			if(arr[i]%2==0){
				sum1 += arr[i];
			}else{
				sum2+= arr[i];
			}
		}
		System.out.println("Odd sum: " + sum2);
		System.out.println("Even sum: " +sum1);
		System.out.println();
	}
}
