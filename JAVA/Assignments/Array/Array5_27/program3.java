import java.util.*;

class Array{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		System.out.print("Enter size of array: ");

		int arr[]=new int[size];

		int flag=0;

		for(int i = 0; i<size;i++){
			arr[i]=sc.nextInt();
		}

		for(int j=0;j<size;j++){

			if(arr[j]!=arr[size-1-j]){

				flag++;

			}else{

				flag= 0;
			}
		}


		if(flag==0){

			System.out.println("the given array is palindrome");
		}else{
			System.out.println("given array is not palindrome");
		}
	}
}
