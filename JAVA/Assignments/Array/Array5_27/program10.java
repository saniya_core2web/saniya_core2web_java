import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

                int size = sc.nextInt();
                
		int arr[]= new int[size];
                
		for(int i=0;i<size;i++){
			System.out.println("Enter elements: ");
                        arr[i]=sc.nextInt();
                }

		for(int i=0; i<size; i++){
			int mul =1;
			while(arr[i]>=1){
				mul*=arr[i];
				arr[i]--;
			}
			System.out.println(mul + " ");
		}
		System.out.println();
	}
}
