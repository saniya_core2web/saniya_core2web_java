import java.util.Scanner;

class TwoDArray{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.println("enter number of rows: ");
                int rows = sc.nextInt();

                System.out.println("enter number of colums: ");
                int cols = sc.nextInt();

                int[][] array = new int[rows][cols];
                System.out.println("enter the elements of array: ");

                for(int i=0; i<rows; i++){
                        for(int j=0; j<cols; j++){
                                array[i][j] = sc.nextInt();
                        }
                }

                int prod=1;


                for(int i=0; i<rows; i++){
                        for(int j=0; j<cols; j++){
                                if(i==j){
					prod*=array[i][j];
				}
			}
		}
		System.out.println("product of primary diagonal: " + prod);
	}
i}
