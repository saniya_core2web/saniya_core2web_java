import java.util.*;

class Array {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int arr[] = {2,5,2,7,8,9,2};

		System.out.print("Enter number : ");
		int num = sc.nextInt();
		int cnt = 0;
		for(int i=0; i<arr.length; i++){

			if(num == arr[i])
				cnt++;
		}
		if(cnt != 0)
			System.out.println("num "+ num + " occurs "+cnt + " times in the array");
		else
			System.out.println("num "+ num + " not found in the array");
	}
}
