import java.util.*;

class Array {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[] = {1,5,9,8,7,6};
		
		System.out.print("Enter number : ");
		int num = sc.nextInt();

		int cnt = 0;
		for(int i=0; i<arr.length; i++){

			if(num == arr[i]){

				System.out.println("num "+ num + " occured at index : "+ i);
				cnt++;
				break;
			}
		}
		if(cnt == 0)
			System.out.println("num "+ num +" not found in the array");

	}
}
