import java.util.*;

class Array {

        public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements of array : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		if(size %2==1 && size >= 5){
			for(int i=0; i<arr.length; i++){
				if(arr[i]%2 == 1)
					System.out.print(arr[i]+ " ");
			}
		}else{
			for(int i=0; i<arr.length; i++){
				if(arr[i]%2 == 0)
					System.out.print(arr[i]+ " ");
			}

		}
		System.out.println();
        }                                                                                             }
