class StringDemo{
        public static void main(String[] args){

                String str1 = "Shashi";
                System.out.println(str1);
                System.out.println(System.identityHashCode(str1));  //SCP

                String str2 = new String("Shashi");
                System.out.println(str2);
                System.out.println(System.identityHashCode(str2)); //SCP
        }
}
