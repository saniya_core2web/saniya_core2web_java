class NumberPattern{
        public static void main(String[] args){
		for(int i=1; i<=4; i++){
			int num=1;
			char ch='A';
			for(int j=1; j<=3; j++){
				if(i%2==1){
					System.out.print(num++ + " ");
				}else{
					System.out.print(ch++ + " ");
				}
				num++;
				ch++;
			}
			System.out.println();
		}
	}
}
