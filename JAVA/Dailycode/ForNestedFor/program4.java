class Pattern{
	public static void main(String[] args){
		int num=10;
		for(int i=1; i<num; i++){
			if(i%2==0){
				System.out.print(i*i*i+"\t");
			}else{
				System.out.print(i*i+"\t");
			}
		}
		System.out.println();
	}
}
