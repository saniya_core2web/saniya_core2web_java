import java.util.*;

class Demo{

	public static void main(String [] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter array size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter array Elements: ");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=sc.nextInt();
		}

		System.out.print("Elements divisible by 3: ");
		int sum=0;
		for(int j=0;j<arr.length;j++){
		
			if(arr[j]%3==0){
			
				System.out.print(arr[j]+"\t");
				sum+=arr[j];
			}
		}
		System.out.println();
		System.out.println("Sum of the elements divisible by 3 is "+sum);

	}
}
