import java.util.*;

class Array {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter array size : ");
                int size = sc.nextInt();

                int arr[] = new int[size];
                System.out.println("Enter elements : ");
                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.next().charAt(0);
		}

                System.out.print("Enter char : ");
		char ch = sc.next().charAt(0);

		for(int i=0; i<arr.length; i++){
			if(ch == arr[i])
				break;
			System.out.println((char)arr[i]);
		}
	}
}
