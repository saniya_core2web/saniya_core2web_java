import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements : ");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		int min = arr[0];
		int max = arr[0];
		for(int i=0; i<arr.length; i++){

			if(min >= arr[i])
				min = arr[i];

			if(max <= arr[i])
				max = arr[i];
		}
		System.out.println("The difference between minimum and maximum element is : " + (max - min));

	}
}
