import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter count of employees : ");
		int size = sc.nextInt();

		int ageArr[] = new int[size];
		for(int i=0; i<size; i++){

			System.out.print("Enter Employee's age : ");
			ageArr[i] = sc.nextInt();
		}
		for(int i=0; i<size; i++){

			System.out.println("Employee's age : " + ageArr[i]);
		}
	}
}
