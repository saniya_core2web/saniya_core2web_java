import java.util.*;

class Array {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){

			System.out.print("Enter element : ");
			arr[i] = sc.nextInt();
		}
		System.out.print("Array : ");
		for(int i=0; i<arr.length; i++)
			System.out.print(arr[i] + " ");
		System.out.println();
	}
}
