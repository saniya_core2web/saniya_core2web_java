import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		for(int i=0; i<size; i++){

			System.out.print("Enter elements : ");
			arr[i] = sc.nextInt();

		}
		for(int i=0; i<size; i++){

			if(i%2 == 0)
				System.out.println(arr[i] + " is an odd indexed element");
		}
	}
}
