import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of Array : ");
		int size = sc.nextInt();
		int sum = 0;

		int arr[] = new int[size];
		for(int i=0; i<size; i++){

			System.out.print("Enter element : ");
			arr[i] = sc.nextInt();
		}
		for(int i=0; i<size; i++){

			if(arr[i]%2 == 1)
				sum += arr[i];
		}
		System.out.println("Sum of odd elements : " + sum);
	}
}
