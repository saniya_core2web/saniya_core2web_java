import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of Array : ");
		int size = sc.nextInt();

		char arr[] = new char[size];
		for(int i=0; i<size; i++){

			System.out.print("Enter character : ");
			arr[i] = (char)sc.next().charAt(0);
		}
		for(int i=0; i<size; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
