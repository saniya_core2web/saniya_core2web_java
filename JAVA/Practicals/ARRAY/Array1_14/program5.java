import java.util.*;

class Array {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of Array : ");
		int size = sc.nextInt();
		int arr[] = new int[10];

		for(int i=0; i<size; i++){

			System.out.print("Enter element : ");
			arr[i] = sc.nextInt();
		}
		for(int i=0; i<size; i++){

			if(arr[i] < 10)
				System.out.println(arr[i] + " is less than 10");
		}
	}
}
