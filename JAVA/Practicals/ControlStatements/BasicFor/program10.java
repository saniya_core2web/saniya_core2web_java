class ForLoop{
	public static void main(String[] args){
		long product = 1;
		for (int i = 1; i <= 10; i++) {
			product *= i;
		}
		System.out.println("The product of the first 10 natural numbers is: " + product);
	}
}

