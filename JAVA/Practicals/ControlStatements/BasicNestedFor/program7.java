class Pattern{
	public static void main(String[] args){
		int rows= 3;
		for(int i=1; i<=rows; i++){
			int ch =96+rows;
			for(int j=1; j<=rows; j++){
				System.out.print((char)ch-- + " ");
			}
			System.out.println();
		}
	}
}
