class Pattern{
	public static void main(String[] args){
		int number =1;
		int rows=3;
		for(int i=0; i<rows; i++){
			int num=number +i;
			for(int j=0; j<rows; j++){
				System.out.print(num++ + " ");
			}
			System.out.println();
		}
	}
}
