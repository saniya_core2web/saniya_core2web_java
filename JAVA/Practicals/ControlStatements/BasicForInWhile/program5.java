class BasicForWhile{
	public static void main(String[] args){
		int num=1;
		while(num<=70){
			if(num%2==1){
				System.out.print(num + " ");
			}
			num++;
		}
	}
}
