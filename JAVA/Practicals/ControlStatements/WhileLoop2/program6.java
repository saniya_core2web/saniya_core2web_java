class WhileLoop{
	public static void main(String[] args){
		int num=234;
		int rem;
		int prod=1;
		while(num>0){
			rem=num%10;
			prod = prod*rem;
			num/=10;
		}
		System.out.println(prod);
	}
}
