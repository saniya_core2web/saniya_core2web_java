class WhileLoop{
	public static void main(String[] args){
		int sum=0;
		int num=256985;
		int rem;
		while(num>0){
			rem =num%10;
			if(rem%2==0){
				sum=sum+rem;
			}
			num/=10;
		}
		System.out.print("Sum of even digits: " +sum);
		System.out.println();
	}
}
