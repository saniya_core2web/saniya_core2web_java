class WhileLoop{
	public static void main(String[] args){
		int num=256985;
		int rem;
		System.out.println("square of odd digits of a given number is: ");
		while(num>0){
			rem=num%10;
			if(rem%2==1){
				System.out.print(rem*rem + " ");
			}
			num/=10;
		}
	}
}
