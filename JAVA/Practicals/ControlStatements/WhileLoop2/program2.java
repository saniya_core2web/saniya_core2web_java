class WhileLoop{
	public static void main(String[] args){
		int num=2569185;
		int rem;
		System.out.println("digits from given number which is not divisible by 3 is: ");
		while(num>0){
			rem=num%10;
			if(rem%3!=0){
				System.out.print(rem + " ");
			}
			num/=10;
		}
	}
}
