class WhileLoop{
	public static void main(String[] args){
		int num=2469185;
		int sum=0;
		int sqr;
		int rem;
		int temp=num;
		while(temp>0){
			rem=temp%10;
			if(temp%2==1){
				sqr =rem*rem;
				sum=sum+sqr;
			}
			temp/=10;
		}
		System.out.println(sum);
	}
}
