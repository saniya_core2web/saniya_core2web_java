

class WhileLoop{
	public static void main(String[] args){
		int num=256985;
		int rem;
		int prod=1;
		while(num>0){
			rem=num%10;
			if(rem%2==1){
				prod=prod*rem;
			}
			num/=10;
		}
		System.out.print("product of odd digits: " +prod);
		System.out.println();
	}
}
