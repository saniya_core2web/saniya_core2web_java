class WhileLoop{
	public static void main(String[] args){
		int num=9367924;
		int sum=0;
		int prod=1;
		int rem;
		while(num>0){
			rem=num%10;
			if(rem%2==0){
				prod=prod*rem;
			}else{
				sum=sum+rem;
			}
			num/=10;
		}
		System.out.println("Sum of odd digits: " +sum);
		System.out.println("product of even digits: " +prod);
	}
}
