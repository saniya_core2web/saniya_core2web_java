class WhileLoop{
        public static void main(String[] args){
                int num=216985;
                int rem;
                System.out.print("Cubes of even digits of a given number is: ");
                while(num>0){
                        rem=num%10;
                        if(rem%2==0){
                                System.out.print(rem*rem*rem + " ");
                        }
                        num/=10;
                }
		System.out.println();
	}
}

