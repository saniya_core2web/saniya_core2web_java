import java.io.*;
class Triangle{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");

		int rows= Integer.parseInt(br.readLine());
		int num=1;

		for(int i=1; i<=rows; i++){
			int temp=num;
			for(int j=rows; j>=i; j--){
				System.out.print(temp++ + " ");
			}
			System.out.println();
			num++;
		}
	}
}
