
import java.io.*;
class Triangle{
	public static void main(String [] args)throws IOException{

		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows: ");

		int rows= Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
			int ch = 65+(rows-1);
			for(int j=i; j<=rows; j++){
				System.out.print((char)ch-- +" ");
			}
			System.out.println();
		}
	}
}
