class While{
	public static void main(String[] args){
		int rem;
		int num=214367689;
		int oddcount=0;
		int evencount=0;
		while(num>0){
			rem=num%10;
			if(rem%2==0){
				evencount++;
			}else{
				oddcount++;
			}
			num/=10;
		}
		System.out.println(" Odd Count: " +oddcount);
		System.out.println(" Even Count: " +evencount);
	}
}
