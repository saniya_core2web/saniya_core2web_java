import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		int ch =64+rows;

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(j==1){
					System.out.print((char)ch + " ");
				}else{
					System.out.print(num + " ");
				}
				num++;
				ch++;
		        }
			System.out.println();
		}
	}
}
