import java.io.*;
class Pattern{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter rows: ");

                int rows = Integer.parseInt(br.readLine());

                for(int i=1; i<=rows; i++){
			 int ch = 64+rows;
                         int num= rows;

                        for(int j=1; j<=rows; j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print(num + " ");
					}else{
						System.out.print((char)ch + " ");
					}
				}else{
					System.out.print((char)ch + " ");
				}
				num--;
				ch--;
				
			}
			System.out.println();
		}
	}
}
