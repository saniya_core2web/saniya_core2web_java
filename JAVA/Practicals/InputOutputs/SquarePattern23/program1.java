import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter rows: ");

		int rows=Integer.parseInt(br.readLine());
		int ch = 64+rows;

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(j==1){
					System.out.print((char)ch + " ");
				}else{
					System.out.print((char)(ch+32) + " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
