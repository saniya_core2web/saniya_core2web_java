import java.util.*;

class Factorial {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int temp = num;
		int fact = 1;
		while(temp>0){

			fact *= temp--;
			
		}
		System.out.println("Factorial of "+ num +" is "+ fact);

	}
}
