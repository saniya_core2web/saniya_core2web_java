import java.util.*;

class Composite{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                int num = sc.nextInt();
                int temp = 1;
                int cnt = 0;
                while(temp<=num) {

                        if(num%temp == 0)
                                cnt++;
                        temp++;
                }
                if(cnt>2)
                        System.out.println(num + " is a composite number");
                else
                        System.out.println(num + " is not a composite number");

        }
}
