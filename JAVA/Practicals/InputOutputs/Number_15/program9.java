import java.util.*;

class Reverse {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		System.out.print("Reverse of "+ num +" is ");
		for(int temp=num; temp>0;){

			int rem = temp % 10;
			System.out.print(rem);
			temp /= 10;
		}
		System.out.println();
	}
}
