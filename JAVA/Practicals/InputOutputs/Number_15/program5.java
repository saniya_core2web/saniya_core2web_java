import java.util.*;

class Composite {

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                int num = sc.nextInt();
                int cnt = 0;
                for(int i=1; i<=num; i++){

                        if(num % i == 0)
                                cnt++;
                }
                if(cnt>2)
                        System.out.println(num + " is a composite number");
                else
                        System.out.println(num + " is not a composite number");
        }
}
