import java.util.*;

class Palindrome {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		int palindrome = 0;
		for(int temp=num; temp>0;){

			int rem = temp%10;
			palindrome = palindrome*10+rem;
			temp /= 10;
		}
		if(palindrome == num)
			System.out.println(num +" is a palindrome number");
		else
			System.out.println(num +" is not a palindrome number");
	}
}
