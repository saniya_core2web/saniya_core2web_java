import java.util.*;

class Pattern {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();    
		
		for(int i=1; i<=rows; i++){
			int num = rows;

			for(int j=rows-1; j>=i; j--)
				System.out.print("    ");
			for(int j=1; j<=i; j++)
				System.out.print(num*j + "  ");
			System.out.println();
		}
	}                                                                                             }
