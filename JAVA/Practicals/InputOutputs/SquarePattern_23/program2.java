import java.util.*;

class Pattern{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int row = sc.nextInt();

		int ch = 96+row;
		
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){
			
				if((i+j)<=row){
				
					System.out.print((char)ch+" ");
				}else{
				
					System.out.print((char)(ch-32)+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
