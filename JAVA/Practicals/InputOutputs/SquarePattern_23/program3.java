import java.util.*;

class Pattern{

	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);

		System.out.print("Rows = ");
		int row = sc.nextInt();

		int ch = 96+row;
		int num=row;

		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){
			
				if(i%2==1 && j%2==1 || i%2==0 && j%2==0){
				
					System.out.print((char)ch+" ");
				}else{
				
					System.out.print(num+" ");
				}
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}
