import java.util.*;

class Pattern{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
		
			int num1=row*row;
			int num2=(row*2-2)*row;
			int diff= i*2;

			for(int j=1;j<=row;j++){
			
				if( i==1 || i==row){
				
					if(j%2==0){
					
						System.out.print("@\t");
					}else{
					
						System.out.print(num1+"\t");
						num1-=diff;
					}
				}else{
				
					 if(j%2==0){

                                                System.out.print("@\t");
                                        }else{

                                                System.out.print(num2+"\t");
                                                num2-=diff;
                                        }
				}
			}
			System.out.println();
		}
	}
}
