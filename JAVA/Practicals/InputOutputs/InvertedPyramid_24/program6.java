import java.util.*;

class Pattern{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Rows = ");
		int row = sc.nextInt();
		int ch = 64+row;
		for(int i=1; i<=row; i++){
		
			for(int s=1; s<i; s++){
			
				System.out.print("\t");
			}

			for(int j=1; j<=(row-i)*2+1; j++){
			
				System.out.print((char)ch+"\t");
			}
			ch--;
			System.out.println();
		}
	}
}
