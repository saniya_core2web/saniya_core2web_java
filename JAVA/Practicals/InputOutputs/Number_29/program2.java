import java.util.*;

class StrongNum{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number: ");
                int num = sc.nextInt();

		int temp=num;

		int sum=0;
		while(num>0){
			int digit=num%10;
			int mul=1;
			
			while(digit>=1){
			mul*=digit;
			digit--;
			}
			
			num/=10;
			sum+=mul;
		}
		if(sum==temp){
			System.out.println(temp + " is a Strong number");
		}else{
			System.out.println(temp + " is not a Strong number");
		}
	}
}
