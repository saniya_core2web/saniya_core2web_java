import java.util.*;
class AutomorphicNum{

        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Number: ");

                int num=sc.nextInt();

		int digit=0;
		int var=0;
		int temp=num;

		while(num>0){
			num/=10;
			digit++;
		}
		int sq =temp*temp;

		while(digit>0){
			int rem =sq%10;
			var = rem + var*10;
			sq/=10;
			digit--;
		}

		int rev=0;

		while(var>0){
			int rem=var%10;
			var/=10;
			rev = rem + rev*10;
		}
	if(rev==temp){
		System.out.println(temp + " is an Automorphic Number");
	}else{
		System.out.println(temp + " is not an Automorphic Number");
	}
	}
}	
