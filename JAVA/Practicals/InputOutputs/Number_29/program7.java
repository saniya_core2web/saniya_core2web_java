import java.util.*;
class HappyNum{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number: ");

		int num=sc.nextInt();

		int result=num;
		int sum=0;
		int rem=0;
		while(num>0){
			rem = num%10;
			sum=sum+(rem*rem);
			num/=10;
		}

		if(sum==1){
			System.out.println(result + " Happy number");
		}else{
			System.out.println(result + " is not happy number");
		}
	}
}
