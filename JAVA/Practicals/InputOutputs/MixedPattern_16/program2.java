import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Number of rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int ch = 64 + rows;
			int num = rows + i-1;
			for(int j=1; j<=rows; j++){

				System.out.print((char)ch + "" + num-- + " ");
			}
			System.out.println();
		}
	}
}
