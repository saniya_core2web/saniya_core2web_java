import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Number of rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int num = rows-i+1;
			int temp = num;
			for(int j=1; j<=i; j++){

				System.out.print(num + " ");
				num +=temp; 
			}
			System.out.println();
		}
	}
}
