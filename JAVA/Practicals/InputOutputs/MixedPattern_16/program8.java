import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int ch = 64 + rows*(rows+1)/2;
		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=i; j--){

				System.out.print((char)ch-- + " ");
			}
			System.out.println();
		}
	}
}
