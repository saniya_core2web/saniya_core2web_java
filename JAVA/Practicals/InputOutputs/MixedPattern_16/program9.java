import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int num = 1;
			int ch = 65 + rows-i;
			for(int j=rows; j>=i; j--){

				if(i%2==0)
					System.out.print((char)ch-- + " ");
				else
					System.out.print(num++ + " ");
			}
			System.out.println();
		}
	}
}
