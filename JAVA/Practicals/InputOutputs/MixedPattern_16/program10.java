
import java.util.*;

class Pattern {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Number = ");
		long num = sc.nextLong();

		long temp = num;
		while(temp>0){

			long rem = temp%10;
			temp /= 10;
			if(rem%2==1)
				System.out.print(rem * rem + " ");
		}
		System.out.println();
	}
}
